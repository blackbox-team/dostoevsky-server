from dostoevsky.models import FastTextSocialNetworkModel
from dostoevsky.tokenization import RegexTokenizer

import re


class Predictor():

    def __init__(self, message_raw="",
                 messages_for_prediction_edge=3,
                 pos_weight=1.5,
                 neg_weight=1.5,
                 neut_weight=1.0):

        self.tokenizer = RegexTokenizer()
        self.model = FastTextSocialNetworkModel(tokenizer=self.tokenizer)

        self.messages_edge = messages_for_prediction_edge
        self.pos_weight = pos_weight
        self.neg_weight = neg_weight
        self.neut_weight = neut_weight

        self.message_raw = message_raw
        self.messages_lst = self.get_messages()
        self.current_msg = ""

    def get_messages(self):
        msg_lst = re.split('(?<=[.!?]) +', self.message_raw)  # split by !, ., ? at the end of the sentence
        if len(msg_lst) > self.messages_edge:
            msg_lst = msg_lst[-self.messages_edge:]
        else:
            msg_lst = self.message_raw
        return msg_lst

    def weight_predictions(self, predictions):
        if(predictions[0]['positive'] > 0.1 or predictions[0]['negative'] > 0.1 or predictions[0]['neutral'] > 0.1):
            predictions[0]['positive'] *= self.pos_weight
            predictions[0]['negative'] *= self.neg_weight
            predictions[0]['neutral'] *= self.neut_weight
        else:
            predictions[0]['positive'] *= 0.0
            predictions[0]['negative'] *= 0.0
            predictions[0]['neutral'] *= 1.0
        return predictions

    def normalize_predictions(self, predictions):
        total = 0.0
        for key, value in predictions[0].items():
            total += value

        for key, value in predictions[0].items():
            predictions[0][key] = value / total
        return predictions

    def delete_unused_keys(self, predictions):
        return [{key: value for key, value in predictions[0].items()
                 if key != 'skip' and key != 'speech'}]

    def get_raw_predictions(self):
        resulting_msg = ""

        for msg in self.messages_lst:
            resulting_msg += msg

        self.current_msg = resulting_msg
        sentiment = self.model.predict([resulting_msg], k=5)
        return sentiment

    def get_final_predictions(self):
        return self.normalize_predictions(self.weight_predictions(self.delete_unused_keys(self.get_raw_predictions())))
