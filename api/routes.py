from flask import request, jsonify

from api import app
from api.predictor.predictor import Predictor
from api.processor.processor import Processor
import json

recommendations = {
    "neg70": [
        "Предложить звонок",
        "Переключить на сотрудника по работе с возражениями",
        "Предложить льготные условия обслуживания по продукту"
    ],
    "neg50": [
        "Предложить звонок",
        "Переключить на сотрудника по работе с возражениями",
        "Предложить компенсацию бонусами/кэшбеком"
    ],
    "neut": [
        "Предложить звонок",
        "Уточнить вопрос",
        "Перевод к специалисту по конкретному вопросу (без необходимости повторять вопрос заново)"
    ],
    "pos50": [
        "Предложить рекомендовать другу",
        "Оценить приложение в AppStore/Google Play",
        "Предложить подписаться на рассылку предложений от банка"
    ],
    "pos70": [
        "Предложить рекомендовать другу",
        "Предложить апгрейд имеющегося продукта",
        "Предложить новый продукт"
    ]
}


@app.route('/predict', methods=['GET', 'POST'])
def add_message():
    print(request.json)
    try:
        text_to_predict = request.json['message']

        try:
            predictor = Predictor(text_to_predict)

            predictor.message_raw = text_to_predict
            sentiment = predictor.get_final_predictions()

            processor = Processor(recommendations, predictor.current_msg, sentiment)
            response = processor.prepare_response()

            print(predictor.get_raw_predictions())
            print(sentiment)
            return json.dumps(response, ensure_ascii=False)

        except Exception:
            return internal_server_error()

    except Exception:
        return bad_request()


@app.errorhandler(400)
def bad_request():
    message = {
        'status': 400,
        'message': 'Bad Request: Please check your data payload.',
    }
    resp = jsonify(message)
    resp.status_code = 400

    return resp

@app.errorhandler(500)
def internal_server_error():
    message = {
        'status': 500,
        'message': 'Internal server error.',
    }
    resp = jsonify(message)
    resp.status_code = 500

    return resp
