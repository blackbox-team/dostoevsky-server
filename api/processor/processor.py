import re


class Processor():

    def __init__(self, recommendations, current_message, sentiment):

        self.recommendations = recommendations
        self.current_message = current_message
        self.sentiment = sentiment

    def get_recommendations(self):
        max_emotion = max(self.sentiment[0], key=lambda key: self.sentiment[0][key])
        emotion_power = self.sentiment[0][max_emotion]


        if emotion_power > 0.7 and max_emotion == "positive":
            recommendations = self.recommendations["pos70"]
        elif emotion_power > 0.5 and max_emotion == "positive":
            recommendations = self.recommendations["pos50"]
        elif emotion_power > 0.7 and max_emotion == "negative":
            recommendations = self.recommendations["neg70"]
        elif emotion_power > 0.5 and max_emotion == "negative":
            recommendations = self.recommendations["neg50"]
        else:
            recommendations = self.recommendations["neut"]

        return recommendations

    def prepare_response(self):
        prepared_response = {
            "message": self.current_message,
            "sentiment": self.sentiment[0],
            "recommendation": self.get_recommendations()
        }
        return prepared_response
